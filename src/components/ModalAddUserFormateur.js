import React from 'react';
import {Button, Form, Input, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalAddUserFormateur extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal : false,
          errorpassword : '',
          userForm : { 
            firstname : '',
            lastname : '',
            email : '',
            password : '',
            confirmPassword : ''
            }
        } 

        this.handleChange = this.handleChange.bind(this);
        this.handleUserStagiaireAdd = this.handleUserStagiaireAdd.bind(this);
        this.toggle = this.toggle.bind(this);
        };

        handleChange(event) {
            this.setState({
                userForm : { 
                    ...this.state.userForm,
                    [event.target.name] : event.target.value
                }
            });
          }

          handleUserStagiaireAdd(event) {
            event.preventDefault();
            const jwtCefimToken = getCefimJwt();
            if(!jwtCefimToken) {
                this.props.history.push('/login');
             }
         
            axios({
                method: 'POST',
                url: 'http://localhost:8080/api/v1/auth/registerFormateur/',
                headers: {'Authorization' : jwtCefimToken},
                data : {
                        firstname : this.state.userForm.firstname,
                        lastname : this.state.userForm.lastname,
                        email : this.state.userForm.email,
                        password : this.state.userForm.password,
                        confirmPassword : this.state.userForm.confirmPassword
                        
                    }           
                }).then((response ) => {
                this.props.getUsersData();
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {
                    this.setState( {
                        ...this.state,
                        errorpassword : error.response.data.message,
                    } );
                   this.setState( {
                       ...this.state.userForm,
                       userForm : {
                           firstname : this.state.userForm.firstname,
                           lastname : this.state.userForm.lastname,
                           email : this.state.userForm.email,
                           password : '',
                           confirmPassword : ''
                       }
                   }

                   )
           
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
        }

    toggle() {
        this.setState({
          modal: !this.state.modal,
          errorpassword : ''
        });
        
        this.setState( {
            ...this.state.userForm,
            userForm : {
                firstname : '',
                lastname : '',
                email : '',
                password : '',
                confirmPassword : ''
            }
        })
      }

        render() {
            return (
              <div> 
              
             
                <Button color="info" onClick={this.toggle}>Ajouter formateur</Button>
                <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                  toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggle}>Ajouter formateur</ModalHeader>
                  <Form onSubmit={this.handleUserStagiaireAdd}>
                  <ModalBody>
                  {(this.state.errorpassword !== '') ? <Alert color="danger">{this.state.errorpassword}</Alert> : ''}
                  
                
                  <Input type="text" name="firstname" id="firstname" value={this.state.userForm.firstname} onChange={this.handleChange} placeholder="Votre prénom" required/>
                 
                  <br />
                
                  <Input type="text" name="lastname" id="lastname" required value={this.state.userForm.lastname} onChange={this.handleChange} placeholder="Votre nom"/>                   
                  <br />

                  <Input type="text" name="email" id="email" required value={this.state.userForm.email} onChange={this.handleChange} placeholder="Votre email"/>   
                  <br />
                
                  <Input type="password" name="password" id="password" required value={this.state.userForm.password} onChange={this.handleChange} placeholder="Votre mot de passe"/>   
                  <br />

                  <Input type="password" name="confirmPassword" id="confirmPassword" required value={this.state.userForm.confirmPassword} onChange={this.handleChange} placeholder="Confirmez votre mot de passe"/>  
                 
                  </ModalBody>
    
                  <ModalFooter>
                    <Button color="info">Ajouter formateur</Button>
                    <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                    
                  </ModalFooter>
                  </Form>
                </Modal>
                
              </div>
            );
          }
}

export default ModalAddUserFormateur