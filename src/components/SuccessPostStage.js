import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Jumbotron, Button, Container, Row, Col } from 'reactstrap';

class SuccessPostStage extends Component { 

    render() {
       
            return (
                <div>
                  <Container>
                      <Row> 
                      <Col sm="12" md={{ size: 10, offset: 1 }}>

                        <Jumbotron>
                          <h1 className="display-3">CEFIM vous remercie !</h1>
                          <p className="lead">Votre offre de stage a été ajoutée.</p>
                          <hr className="my-2" />
                          <p>Notre équipe de formateurs publiera votre offre dans les meilleurs délais. </p>
                          <p className="lead">
                            
                          <Link to="/"><Button color="primary">Retour vers la page d'accueil</Button></Link> 
                          </p>
                        </Jumbotron>
                        </Col>
                      </Row>
                  </Container>                       
                </div>
              );
    }
}

export default SuccessPostStage;