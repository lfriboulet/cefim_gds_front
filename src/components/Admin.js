import React from 'react';
import MenuAdmin from './MenuAdmin';
import {Main} from './Main';
import {getCefimJwt} from '../helpers/jwt';
import jwt_decode from 'jwt-decode';
import Loader from 'react-loader-spinner';

export default class Admin extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            loaded : false
        }
    }

    componentWillMount() {
        
        const jwtCefimToken = getCefimJwt();
        
        if (jwtCefimToken === null) {
            window.location = "/login";
            return;
        } 

        const decoded = jwt_decode(jwtCefimToken);
        const currentTime = Date.now().valueOf() / 1000;

        if (currentTime > decoded.exp) {
            localStorage.removeItem('cefimtoken');
            window.location = "/login";
            return;
        } else {
            this.setState({loaded : true});
        }
    }

    render() {
        if (!this.state.loaded) {
            return(
                <div className="loader">
                    <Loader 
                    type="ThreeDots"
                    color="orange"
                    height="100"	
                    width="100"
                    />  
                </div>
    
               );
        } else {
        return (
        <div>
              <MenuAdmin/>
              <Main/>
          </div>
        )
        }
    }
}

  
