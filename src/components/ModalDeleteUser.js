import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalDeleteUser extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modal : false,
            userForm : this.props.userData,
            errorUser : ''
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleUserDelete = this.handleUserDelete.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    handleUserDelete() {
        
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }

        axios({
            method: 'DELETE',
            url: 'http://localhost:8080/api/v1/auth/user/' + this.state.userForm.id,
            headers: {'Authorization' : jwtCefimToken},
           
            }).then((response ) => {
            this.props.getUsersData();
            this.toggle();
                
            }).catch((error) => {
            
            if (error.response) {
                this.setState( {
                    ...this.state,
                    errorUser : error.response.data.message 
                } )
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });
    }

    render() {
        return (
                <div>
    <Button color="danger" onClick={this.toggle}>Supprimer</Button>
    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
        <ModalHeader toggle={this.toggle}>Suppression de l'utilisateur {this.state.userForm.firstname} {this.state.userForm.lastname} </ModalHeader>
        <ModalBody>
        {(this.state.errorUser !== '') ? <Alert color="danger">{this.state.errorUser}</Alert> : ''}
        <h6>Confirmer la suppression de cet utilisateur  <strong>{this.state.userForm.email}</strong> ? </h6>
        </ModalBody>
        <ModalFooter>
        <Button color="info" onClick={this.handleUserDelete}>Supprimer</Button>
        <Button color="secondary" onClick={this.toggle}>Annuler</Button>
        </ModalFooter>
    </Modal>
    </div>
  )
    }

}

export default ModalDeleteUser