import React from 'react';
import {Alert, Button, Form, Input, Modal, ModalHeader, ModalBody, ModalFooter, Label,FormGroup} from 'reactstrap';
import axios from "axios";

class ModalAddStagePartner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {stageForm : {
            companyName : '',
            companyAddress : '',
            companyZipCode : '',
            companyCity : '',
            companyContactName : '',
            companyContactEmail : '',
            companyContactPhone : '',
            targetTraining : '',
            jobTitle : '',
            jobDescription : ''
        },
        errorStageForm : {
            companyName : '',
            companyAddress : '',
            companyZipCode : '',
            companyCity : '',
            companyContactName : '',
            companyContactEmail : '',
            companyContactPhone : '',
            targetTraining : '',
            jobTitle : '',
            jobDescription : ''
        },
        errorHTTP : {
            status : '',
            message : '',
            error : ''
        },
        trainingData : [],
        modal: false
        };
    
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeTraining = this.handleChangeTraining.bind(this);
        this.handleAddStage = this.handleAddStage.bind(this);
        this.getTrainingsData = this.getTrainingsData.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
        this.setState( {
            ...this.state.stageForm,
            stageForm : {
                companyName : '',
                companyAddress : '',
                companyZipCode : '',
                companyCity : '',
                companyContactName : '',
                companyContactEmail : '',
                companyContactPhone : '',
                targetTraining : '',
                jobTitle : '',
                jobDescription : ''
            }
        })
      }
    
    handleChange(event) {
        this.setState({
            stageForm : { 
                ...this.state.stageForm,
                [event.target.name] : event.target.value
            }
        });
    }
    
    handleChangeTraining(event) {
        this.setState({
            stageForm : {
                ...this.state.stageForm,
             targetTraining : {id : event.target.value}}
            })
    }

    handleAddStage(event) {
        event.preventDefault();
        axios.post('http://localhost:8080/api/v1/stage', this.state.stageForm )
        .then(res => {
            this.toggle();
            window.location = "/success";
        }).catch((error) => {
          // Error
        if (error.response.data.status === 500) {
            this.setState({
                errorHTTP : {
                    ...this.state.errorHTTP,
                    status : error.response.data.status,
                    message : error.response.data.message,
                    error : error.response.data.error
                }
            })
        } else {
            (error.response.data.errors).forEach((value) => {
                  
                this.setState({
                    errorStageForm : {
                        ...this.state.errorStageForm,
                            [value.field] : value.defaultMessage
                    }
                })
            }) 
        }
      });
    }

    getTrainingsData() {
        axios({
          method: 'GET',
          url: 'http://localhost:8080/api/v1/trainings'
      
          }).then((response ) => {
              this.setState({
                trainingData : response.data
              });
   
          }).catch((error) => {
              // Error
            if (error.response.data.status) {
                this.setState({
                    errorHTTP : {
                        ...this.state.errorHTTP,
                        status : error.response.data.status,
                        message : error.response.data.message,
                        error : error.response.data.error
                    }
                })
          }
      });
      }

      componentDidMount() {
        this.getTrainingsData();
    }

    render() {
        return (
        <div>
            <Button color="danger" onClick={this.toggle}>Accéder au formulaire</Button>
           
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>Compléter le formulaire</ModalHeader>
            {
                     (this.state.errorStageForm.targetTraining !== '') || (this.state.errorHTTP.status !== '')
                     ?
                      <Alert color="danger">
                      <div>
                        <strong>Veuillez contacter votre administrateur système.</strong> <hr /> 
                        <strong>Code erreur : </strong>{this.state.errorHTTP.status} - {this.state.errorHTTP.error}<br /> 
                        <strong>Message erreur : </strong>{this.state.errorHTTP.message} <br /> 
                      </div>
                      </Alert> 
                     :
                     <span></span>
                } 
            <Form onSubmit={this.handleAddStage}>
            <ModalBody>       
           
            <FormGroup>
                <Label for="companyContactName">Nom contact</Label>
                <Input type="text" name="companyContactName" id="companyContactName" required onChange={this.handleChange} placeholder="Prénom, nom du contact" />
                {
                     (this.state.errorStageForm.companyContactName !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyContactName}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }  
            </FormGroup>
        
            <FormGroup>
                <Label for="companyContactEmail">Email contact</Label>
                <Input type="text" name="companyContactEmail" id="companyContactEmail" required onChange={this.handleChange} placeholder="Email du contact" />            
                {
                     (this.state.errorStageForm.companyContactEmail !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyContactEmail}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }
            </FormGroup>
     
            <FormGroup>
                <Label for="companyContactPhone">Téléphone contact</Label>
                <Input type="text" name="companyContactPhone" id="companyContactPhone" onChange={this.handleChange} placeholder="Téléphone du contact" />
                {
                     (this.state.errorStageForm.companyContactPhone !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyContactPhone}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }               
            </FormGroup>
        
            <FormGroup>
                <Label for="companyName">Société</Label>
                <Input type="text" name="companyName" id="companyName" required onChange={this.handleChange} placeholder="Nom de la société" />
                {
                     (this.state.errorStageForm.companyName !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyName}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                } 
            </FormGroup>
    
            <FormGroup>
                <Label for="companyAddress">Adresse</Label>
                <Input type="text" name="companyAddress" id="companyAddress" required onChange={this.handleChange} placeholder="Adresse postale" />
                    {
                     (this.state.errorStageForm.companyAddress !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyAddress}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }   
            </FormGroup>
 
            <FormGroup>
                <Label for="companyZipCode">Code postal</Label>
                <Input type="text" name="companyZipCode" id="companyZipCode" required onChange={this.handleChange} placeholder="Code postal" />
                {
                     (this.state.errorStageForm.companyZipCode !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyZipCode}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                } 

            </FormGroup>

            <FormGroup>
            <Label for="companyCity">Ville</Label>
            <Input type="text" name="companyCity" id="companyCity" required onChange={this.handleChange} placeholder="Ville" />
            {
                     (this.state.errorStageForm.companyCity !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.companyCity}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }  
            </FormGroup>
    
            <FormGroup>
                <Label for="targetTraining">Formation</Label>
                <Input type="select" name="targetTraining" id="targetTraining" required onChange={this.handleChangeTraining} >
                    
                    <option value = ""> Sélectionner une formation ... </option>
                    {
                      this.state.trainingData.map((training, index) =>
                        <option key = {training.id }value={training.id}> {training.trainingName} </option>
                            )   
                    }
                    </Input>
                
            </FormGroup>
    
            <FormGroup>
                <Label for="jobTitle">Titre du stage</Label>
                <Input type="text" name="jobTitle" id="jobTitleModal" required onChange={this.handleChange} placeholder="Intitulé du stage" />
                {
                     (this.state.errorStageForm.jobTitle !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.jobTitle}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }   
            </FormGroup>
        
            <FormGroup>
                <Label for="jobDescription">Description du stage</Label>
                <Input type="textarea" name="jobDescription" id="jobDescriptionModal" required onChange={this.handleChange} placeholder="Description du stage" />
                {
                     (this.state.errorStageForm.jobDescription !== '') 
                     ?
                        <div>
                            <br/>
                            <Alert color="danger">
                            {this.state.errorStageForm.jobDescription}
                            </Alert> 
                        </div>
                     :
                     <span></span>
                }  
            </FormGroup>
      
            </ModalBody>
            <ModalFooter>
                <Button color="info">Envoyer</Button>{'   '}
                <Button color="secondary" onClick={this.toggle}>Annuler</Button>
            </ModalFooter>
            </Form>
            </Modal>
            
      </div>
        )
    }
}

export default ModalAddStagePartner