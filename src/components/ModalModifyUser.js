import React from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Input, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalModifyUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal : false,
          userForm : this.props.userData,
          errorUser : ''
            } 

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeRole = this.handleChangeRole.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleUserUpdate = this.handleUserUpdate.bind(this);
        };
    
        handleChange(event) {
            this.setState({
                userForm : { 
                    ...this.state.userForm,
                    [event.target.name] : event.target.value
                }
            });
          }

          handleChangeRole(event) {
            this.setState({
                userForm : {
                    ...this.state.userForm,
                 roles : [{id : event.target.value}]}
                });
        }
        
          toggle() {
            this.setState({
              ...this.state,
              modal: !this.state.modal
            });
            
          }

          handleUserUpdate(event) {
            event.preventDefault();
            const jwtCefimToken = getCefimJwt();
            if(!jwtCefimToken) {
                this.props.history.push('/login');
             }
         
            axios({
                method: 'PUT',
                url: 'http://localhost:8080/api/v1/auth/user/' + this.state.userForm.id,
                headers: {'Authorization' : jwtCefimToken},
                data : {
                        id : this.state.userForm.id,
                        firstname : this.state.userForm.firstname,
                        lastname : this.state.userForm.lastname,
                        email : this.state.userForm.email,
                        roles : this.state.userForm.roles,
                        active: this.state.userForm.active,
                        password: ''
                        
                    }           
                }).then((response ) => {
                this.props.getUsersData();
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {
                  this.setState( {
                    ...this.state,
                    errorUser : error.response.data.message 
                } )
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
        }

        render() {
            return (
              <div> 
              
              
                <Button color="warning" onClick={this.toggle}>Modifier</Button>
                <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                  toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggle}>Modification de l'utilisateur {this.state.userForm.firstname} {this.state.userForm.lastname}  </ModalHeader>
                  
                  <Form  onSubmit={this.handleUserUpdate}>
                  <ModalBody>

                  {(this.state.errorUser !== '') ? <Alert color="danger">{this.state.errorUser}</Alert> : ''}

                  <Label for="firstname">Prénom</Label>
                  <Input type="text" name="firstname" id="firstname" required value={this.state.userForm.firstname} onChange={this.handleChange}/>

                  <Label for="lastname">Nom</Label>
                  <Input type="text" name="lastname" id="lastname" required value={this.state.userForm.lastname} onChange={this.handleChange}/>                   
                  

                  <Label for="email">Email</Label>
                  <Input type="text" name="email" id="email" required value={this.state.userForm.email} onChange={this.handleChange}/>   

                  <Label for="roles">Rôle</Label> 
                  {
                    this.state.userForm.roles.map((role, index) =>
                  <Input type="select" name="roles" id="roles" required key={role.id} value={role.id} onChange={this.handleChangeRole}>
                    <option value="1">Utilisateur</option>
                    <option value="2">Administrateur</option>
                  </Input> 
                  )
                    }           
                  </ModalBody>
    
                  <ModalFooter>
                    <Button color="info">Mise à jour</Button>
                    <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                  </ModalFooter>
                  </Form>
                </Modal>
                
              </div>
            );
          }


}

export default ModalModifyUser