import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalDeleteStage extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modal : false,
            stageForm : this.props.data
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleStageDelete = this.handleStageDelete.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    handleStageDelete() {
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }

        axios({
            method: 'DELETE',
            url: 'http://localhost:8080/api/v1/stage/' + this.state.stageForm.id,
            headers: {'Authorization' : jwtCefimToken},
           
            }).then((response ) => {
            this.props.getStagesData();
            this.toggle();
                
            }).catch((error) => {
            
            if (error.response) {
                console.log(error.response);         
                console.log(error.response.data)
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });
    }  

    render() {
        return (
                  <div>
        <Button color="danger" onClick={this.toggle}>Supprimer</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Suppression de l'offre n° {this.state.stageForm.id}</ModalHeader>
          <ModalBody>
            <h6>Confirmer la suppression du stage <strong>{this.state.stageForm.jobTitle}</strong> publiée par la société <strong>{this.state.stageForm.companyName}</strong> ? </h6>

          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={this.handleStageDelete}>Supprimer</Button>
            <Button color="secondary" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
        </Modal>
      </div>
        )
    }

}

export default ModalDeleteStage