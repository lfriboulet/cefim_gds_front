import React, { Component } from 'react';
import Login from './Login';
import SuccessPostStage from './SuccessPostStage';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import PostStage from './PostStage';
import Protected from './Protected';
import Error404 from './Error404';
import Admin from './Admin';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: undefined
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path = "/" exact component = {PostStage} />
          <Route exact path = '/success'  component = {SuccessPostStage} />
          <Route exact path = "/login"  component = { Login } />
          <Route exact path = "/protected"  component = {Protected} />
          <Route exact path = '/stages'  component = {Admin} />
          <Route exact path = "/users"  component = { Admin } />
          <Route exact path = "/formations"  component = {Admin} />
          <Route exact path = "*"  component = {Error404} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
