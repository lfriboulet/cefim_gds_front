import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalVisibleTraining extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modal : false,
            trainingForm : this.props.dataTraining
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleTrainingVisible = this.handleTrainingVisible.bind(this);
        this.handleTrainingInvisible = this.handleTrainingInvisible.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

      handleTrainingVisible() {
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }
         
            axios({
                method: 'PUT',
                url: 'http://localhost:8080/api/v1/training/' + this.state.trainingForm.id,
                headers: {'Authorization' : jwtCefimToken},
                data : {
                    id : this.state.trainingForm.id,
                    trainingName: this.state.trainingForm.trainingName,
                    actived : true 
                    }           
                }).then((response ) => {
                console.log(response.data);
                window.location.reload();
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {
                    console.log(error.response);         
                    console.log(error.response.data)
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
    } 


    handleTrainingInvisible() {

        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }
      
            axios({
                method: 'PUT',
                url: 'http://localhost:8080/api/v1/training/' + this.state.trainingForm.id,
                headers: {'Authorization' : jwtCefimToken},
                data : {
                    id : this.state.trainingForm.id,
                    trainingName : this.state.trainingForm.trainingName,
                    actived : false
                    }           
                }).then((response ) => {
                console.log(response.data);
                window.location.reload();
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {
                    console.log(error.response);         
                    console.log(error.response.data)
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
    } 

    render() {
        return (
                  <div>
                      {(this.state.trainingForm.actived) 
                      ? 
                    <Button color="primary" onClick={this.toggle}>Désactiver</Button>
                      :
                    <Button color="success" onClick={this.toggle}>Activer</Button>}
                    
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Validation de la formation {this.state.trainingForm.trainingName}</ModalHeader>
                    <ModalBody>
                    {(this.state.trainingForm.actived) 
                      ?
                    <h6>Confirmer de masquer la formation <strong>{this.state.trainingForm.trainingName}</strong> ?</h6>
                    :
                    <h6>Confirmer la visibilité de la formation <strong>{this.state.trainingForm.trainingName}</strong> ?</h6>
                    }
                    </ModalBody>
                    <ModalFooter>
                    {(this.state.trainingForm.actived) 
                      ?
                        <Button color="info" onClick={this.handleTrainingInvisible}>Invisible</Button>
                      :
                      <Button color="info" onClick={this.handleTrainingVisible}>Visible</Button>
                    }
                        <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                    </ModalFooter>
                    </Modal>
      </div>
        )
    }


}

export default ModalVisibleTraining