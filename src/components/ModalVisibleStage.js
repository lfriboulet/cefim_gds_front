import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalVisibleStage extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modal : false,
            stageForm : this.props.data
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleStageVisible = this.handleStageVisible.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    handleStageVisible() {
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }
         
            axios({
                method: 'PUT',
                url: 'http://localhost:8080/api/v1/stage/' + this.state.stageForm.id,
                headers: {'Authorization' : jwtCefimToken},
                data : {
                    id : this.state.stageForm.id,
                    companyName: this.state.stageForm.companyName,
                    companyAddress : this.state.stageForm.companyAddress,
                    companyZipCode : this.state.stageForm.companyZipCode,
                    companyCity : this.state.stageForm.companyCity,
                    companyContactName : this.state.stageForm.companyContactName,
                    companyContactEmail : this.state.stageForm.companyContactEmail,
                    companyContactPhone : this.state.stageForm.companyContactPhone,
                    jobTitle : this.state.stageForm.jobTitle,
                    jobDescription : this.state.stageForm.jobDescription,
                    visibled : true,
                    targetTraining : {
                        id : this.state.stageForm.targetTraining.id
                        } 
                    }           
                }).then((response ) => {
                this.props.getStagesData();
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {
                    console.log(error.response);         
                    console.log(error.response.data)
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
    }  

    render() {
        return (
                  <div>
        <Button color="success" onClick={this.toggle}>VALIDER</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Validation de l'offre n° {this.state.stageForm.id}</ModalHeader>
          <ModalBody>
            <h6>Confirmer la visibilité du stage <strong>{this.state.stageForm.jobTitle}</strong> publiée par la société <strong>{this.state.stageForm.companyName}</strong> ? </h6>

          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={this.handleStageVisible}>Visible</Button>
            <Button color="secondary" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
        </Modal>
      </div>
        )
    }

}

export default ModalVisibleStage