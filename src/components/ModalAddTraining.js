import React from 'react';
import {Button, Form, Input, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalAddTraining extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            trainingForm : {
                trainingName : ''
            },
            errorTraining : '',
            modal : false 
        }
    
        this.handleChange = this.handleChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleAddTraining = this.handleAddTraining.bind(this);
    }

    handleChange(event) {
        this.setState({
            trainingForm : { 
                ...this.state.trainingForm,
                [event.target.name] : event.target.value
            }
        });
      }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
                
        this.setState( {
            ...this.state.trainingForm,
            trainingForm : {
                trainingName : ''
            }
        })
      }
    
    handleAddTraining(event) {
        event.preventDefault();
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
         }
     
        axios({
            method: 'POST',
            url: 'http://localhost:8080/api/v1/training/',
            headers: {'Authorization' : jwtCefimToken},
            data : {
                trainingName: this.state.trainingForm.trainingName,
                actived : this.state.trainingForm.actived
                }           
            }).then((response ) => {
            this.props.getTrainingsData();
            this.toggle();        
            }).catch((error) => {
            
            if (error.response) {
                this.setState( {
                    ...this.state,
                    errorTraining : error.response.data.message 
                } )
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });
            //window.location.reload();
            //this.props.getTrainingsData();
    }
    

    render() {
        return (               
         <div>
            <Button color="primary" onClick={this.toggle}>Ajouter formation</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>Ajouter une formation</ModalHeader>
                <Form onSubmit={this.handleAddTraining}>
                <ModalBody>

                      {(this.state.errorTraining !== '') ? <Alert color="danger">{this.state.errorTraining}</Alert> : ''}

                        <Input type="text" name="trainingName" id="trainingName" required value={this.state.trainingForm.trainingName} onChange={this.handleChange} placeholder="Nom de la formation"/>
                        <br />
                        <Input type="select" name="actived" id="actived" required value={this.state.trainingForm.actived} onChange={this.handleChange} >
                            <option value="true">Activer</option>
                            <option value="false">Désactiver</option>
                        </Input>  

                </ModalBody>
                <ModalFooter>
                
                <Button color="info">Ajouter formation</Button>
                <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                </ModalFooter>
                </Form>
            </Modal>
        </div>
        )
    }

}

export default ModalAddTraining;