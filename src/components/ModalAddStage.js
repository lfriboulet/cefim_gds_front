import React from 'react';
import {Button, Form, Input, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalAddStage extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            stageForm : {
                companyName : '',
                companyAddress : '',
                companyZipCode : '',
                companyCity : '',
                companyContactName : '',
                companyContactEmail : '',
                companyContactPhone : '',
                targetTraining : '',
                jobTitle : '',
                jobDescription : ''
            },
            errorStageForm : {
                companyName : '',
                companyAddress : '',
                companyZipCode : '',
                companyCity : '',
                companyContactName : '',
                companyContactEmail : '',
                companyContactPhone : '',
                targetTraining : '',
                jobTitle : '',
                jobDescription : ''
            },
            trainingData : [],
            collapse: false,
            modal : false 
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeTraining = this.handleChangeTraining.bind(this);
        this.handleAddStage = this.handleAddStage.bind(this);
        this.getTrainingsData = this.getTrainingsData.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
        this.setState( {
            ...this.state.stageForm,
            stageForm : {
                companyName : '',
                companyAddress : '',
                companyZipCode : '',
                companyCity : '',
                companyContactName : '',
                companyContactEmail : '',
                companyContactPhone : '',
                targetTraining : '',
                jobTitle : '',
                jobDescription : ''
            }
        })
      }
    
    handleChange(event) {
        this.setState({
            stageForm : { 
                ...this.state.stageForm,
                [event.target.name] : event.target.value
            }
        });
    }
    
    handleChangeTraining(event) {
        
        this.setState({
            stageForm : {
                ...this.state.stageForm,
             targetTraining : {id : event.target.value}}
            })
    }

    handleAddStage(event) {
        event.preventDefault();
        axios.post('http://localhost:8080/api/v1/stage', this.state.stageForm )
        .then(res => {
          this.props.getStagesData();
          this.toggle();
        }).catch((error) => {
          // Error
          if (error.response) {
              (error.response.data.errors).forEach((value) => {
                  
                  this.setState({
                      errorStageForm : {
                          ...this.state.errorStageForm,
                              [value.field] : value.defaultMessage
                      }
                  })
              })           
              //console.log(error.response.data)
              //console.log(error.response.status);
              //console.log(error.response.headers);
          } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              //console.log(error.request);
          } else {
              // Something happened in setting up the request that triggered an Error
              //console.log('Error', error.message);
          }
          //console.log(error.config);
      });

    }

    getTrainingsData() {
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }
        axios({
          method: 'GET',
          url: 'http://gdspreprod.cefim-formation.org:8080/api/v1/trainings',
          headers: {'Authorization' : jwtCefimToken} 
          }).then((response ) => {
              this.setState({
                trainingData : response.data
              });
            
          }).catch((error) => {
          
          if (error.response) {
              console.log(error.response);         
              console.log(error.response.data)
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              console.log(error.request);
          } else {
              console.log("Error ---->", error.message);
          }
          console.log(error.config);
      });
      }

      componentDidMount() {
          this.getTrainingsData();
      }

    render() {
        return (               
         <div>
            <Button color="primary" onClick={this.toggle}>Ajouter stage</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>Ajouter un stage</ModalHeader>
                <Form onSubmit={this.handleAddStage}>
                <ModalBody> 

                    <Input type="text" name="companyName" id="companyName" required onChange={this.handleChange} placeholder="Nom de la société" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyName}</span>  
                    <br/>

                    <Input type="text" name="companyAddress" id="companyAddress" required onChange={this.handleChange} placeholder="Adresse postale" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyAddress}</span>  
                    <br/>
                    
                    <Input type="text" name="companyZipCode" id="companyZipCode" required onChange={this.handleChange} placeholder="Code postal" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyZipCode}</span>  
                    <br/>
                
                    <Input type="text" name="companyCity" id="companyCity" required onChange={this.handleChange} placeholder="Ville" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyCity}</span>  
                    <br/>
                
                    <Input type="text" name="companyContactName" id="companyContactName" required onChange={this.handleChange} placeholder="Prénom, nom du contact" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyContactName}</span>  
                    <br/>

                    <Input type="text" name="companyContactEmail" id="companyContactEmail" required onChange={this.handleChange} placeholder="Email du contact" />            
                    <span className="badge badge-danger">{this.state.errorStageForm.companyContactEmail}</span>  
                    <br/>

                    <Input type="text" name="companyContactPhone" id="companyContactPhone" onChange={this.handleChange} placeholder="Téléphone du contact" />
                    <span className="badge badge-danger">{this.state.errorStageForm.companyContactPhone}</span>  
                    <br/>

                    <Input type="select" name="targetTraining" id="targetTraining" required onChange={this.handleChangeTraining} >
                    
                    <option value = ""> Sélectionner une formation ... </option>
                    {
                      this.state.trainingData.map((training, index) =>
                        <option key = {training.id }value={training.id}> {training.trainingName} </option>

                            )   
                        }
                    </Input>
                    <span className="badge badge-danger">{this.state.errorStageForm.targetTraining}</span>  
                    <br/>

                    <Input type="text" name="jobTitle" id="jobTitleModal" required onChange={this.handleChange} placeholder="Intitulé du stage" />
                    <span className="badge badge-danger">{this.state.errorStageForm.jobTitle}</span>  
                    <br/>

                    <Input type="textarea" name="jobDescription" id="jobDescriptionModal" required onChange={this.handleChange} placeholder="Description du stage" />
                    <span className="badge badge-danger">{this.state.errorStageForm.jobDescription}</span>  
                
                </ModalBody>
                <ModalFooter>
                <Button color="info">Ajouter stage</Button>
                <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                </ModalFooter>
                </Form>
            </Modal>
        </div>
        )
    }

}

export default ModalAddStage;