import React from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalModifyStage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal : false,
      stageForm : this.props.data,
      errorStageForm : {
        companyName : '',
        companyAddress : '',
        companyZipCode : '',
        companyCity : '',
        companyContactName : '',
        companyContactEmail : '',
        companyContactPhone : '',
        targetTraining : '',
        jobTitle : '',
        jobDescription : ''
    },
    trainingData : [], 
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeTraining = this.handleChangeTraining.bind(this);
    this.toggle = this.toggle.bind(this);
    this.handleStageUpdate = this.handleStageUpdate.bind(this);
    this.getTrainingsData = this.getTrainingsData.bind(this);
  }

  handleChange(event) {
    this.setState({
        stageForm : { 
            ...this.state.stageForm,
            [event.target.name] : event.target.value
        }
    });
  }

  handleChangeTraining(event) {
    
    this.setState({
        stageForm : {
            ...this.state.stageForm,
         targetTraining : {id : event.target.value}}
        })
}

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  getTrainingsData() {
    const jwtCefimToken = getCefimJwt();
    if(!jwtCefimToken) {
        this.props.history.push('/login');
    }
    axios({
      method: 'GET',
      url: 'http://localhost:8080/api/v1/trainings',
      headers: {'Authorization' : jwtCefimToken} 
      }).then((response ) => {
          this.setState({
            trainingData : response.data
          });
        
      }).catch((error) => {
      
      if (error.response) {
          console.log(error.response);         
          console.log(error.response.data)
          console.log(error.response.status);
          console.log(error.response.headers);
      } else if (error.request) {
          console.log(error.request);
      } else {
          console.log("Error ---->", error.message);
      }
      console.log(error.config);
  });
  }

  componentDidMount() {
      this.getTrainingsData();
  }

  handleStageUpdate(event) {
    event.preventDefault();
    const jwtCefimToken = getCefimJwt();
    if(!jwtCefimToken) {
    this.props.history.push('/login');
}
 
    axios({
        method: 'PUT',
        url: 'http://localhost:8080/api/v1/stage/' + this.state.stageForm.id,
        headers: {'Authorization' : jwtCefimToken},
        data : {
            id : this.state.stageForm.id,
            companyName: this.state.stageForm.companyName,
            companyAddress : this.state.stageForm.companyAddress,
            companyZipCode : this.state.stageForm.companyZipCode,
            companyCity : this.state.stageForm.companyCity,
            companyContactName : this.state.stageForm.companyContactName,
            companyContactEmail : this.state.stageForm.companyContactEmail,
            companyContactPhone : this.state.stageForm.companyContactPhone,
            jobTitle : this.state.stageForm.jobTitle,
            jobDescription : this.state.stageForm.jobDescription,
            visibled : false,
            targetTraining : {
                id : this.state.stageForm.targetTraining.id
                } 
            }           
        }).then((response ) => {
        this.props.getStagesData();
        this.toggle();
        }).catch((error) => {
        if (error.response) {
            console.log(error.response);         
            console.log(error.response.data)
            console.log(error.response.status);
            console.log(error.response.headers);
        } else if (error.request) {
            console.log(error.request);
        } else {
            console.log("Error ---->", error.message);
        }
        console.log(error.config);
    });
}

  render() {
    return (
      <div>
      
        <Button color="warning" onClick={this.toggle}>Modifier</Button>
        <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
          toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Modification de l'offre n° {this.state.stageForm.id} </ModalHeader>
          <Form onSubmit={this.handleStageUpdate}>
          <ModalBody>

          <Input type="text" name="companyName" id="companyName" required value={this.state.stageForm.companyName} onChange={this.handleChange}/>
          <span className="badge badge-danger">{this.state.errorStageForm.companyName}</span> 
          <br />

          <Input type="text" name="companyAddress" id="companyAddress" required value={this.state.stageForm.companyAddress} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.companyAddress}</span>
           <br />

          <Input type="text" name="companyZipCode" id="companyZipCode" required value={this.state.stageForm.companyZipCode} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.companyZipCode}</span> 
           <br />

          <Input type="text" name="companyCity" id="companyCity" required value={this.state.stageForm.companyCity} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.companyCity}</span>  
           <br />

          <Input type="text" name="companyContactName" id="companyContactName" required value={this.state.stageForm.companyContactName} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.companyContactName}</span>  
           <br />

          <Input type="text" name="companyContactEmail" id="companyContactEmail" required value={this.state.stageForm.companyContactEmail} onChange={this.handleChange} />            
          <span className="badge badge-danger">{this.state.errorStageForm.companyContactEmail}</span> 
           <br />

          <Input type="text" name="companyContactPhone" id="companyContactPhone" value={this.state.stageForm.companyContactPhone} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.companyContactPhone}</span>  
          <br />

          <Input type="select" id="targetTraining" required  value={this.state.stageForm.targetTraining.id} name={this.state.stageForm.targetTraining.trainingName} onChange={this.handleChangeTraining} >
          
              {
                      this.state.trainingData.map((training, index) =>
                        <option key = {training.id }value={training.id}> {training.trainingName} </option>

                            )   
                        }
          </Input>
          <span className="badge badge-danger">{this.state.errorStageForm.targetTraining}</span> 
           <br />

          <Input type="text" name="jobTitle" id="jobTitleModal" required value={this.state.stageForm.jobTitle} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.jobTitle}</span> 
           <br />

          <Input type="textarea" name="jobDescription" id="jobDescriptionModal" required value={this.state.stageForm.jobDescription} onChange={this.handleChange} />
          <span className="badge badge-danger">{this.state.errorStageForm.jobDescription}</span>  

          </ModalBody>
          <ModalFooter>
            <Button color="info">Mise à jour</Button>
            <Button color="secondary" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
          </Form>
        </Modal>
        
      </div>
    );
  }
}

export default ModalModifyStage;