import React, { Component } from 'react';
import '../css/Error404.css';

export default class Error404 extends Component {

    render() {
        return (
	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>Oops!</h1>
				<h2>404 - Cette page n'existe pas</h2>
			</div>
			<a href="/login">Aller vers la page d'accueil</a>
		</div>
	</div>
        )
    }
}