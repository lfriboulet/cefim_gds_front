import React from "react";
import Stages from './Stages';
import Trainings from './Trainings';
import Users from './Users';
import '../css/Main.css';

import {Route} from "react-router-dom";

export const Main = () => <main>
    <Route path="/stages" component={Stages}/>
    <Route path="/users" component={Users}/>
    <Route path="/formations" component={Trainings}/>
</main>;