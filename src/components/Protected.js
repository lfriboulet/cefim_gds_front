import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import "../css/Protected.css";
import cefimLogo from '../img/cefimLogo.png';
import {Card, CardBody, CardTitle, CardText, CardSubtitle, Container, Row, Col, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";
import jwt_decode from 'jwt-decode';

class Protected extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded : false,
            stageData : [],
            jwtToken : {email : '',
                        role : ''}
        }
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        const jwtCefimToken = getCefimJwt();
        if(jwtCefimToken) {
            localStorage.removeItem('cefimtoken');
            window.location = "/login";
        }
    }

    componentWillMount() {
        
        const jwtCefimToken = getCefimJwt();
        
        if (jwtCefimToken === null) {
            window.location = "/login";
            return;
        } 

        const decoded = jwt_decode(jwtCefimToken);
        const currentTime = Date.now().valueOf() / 1000;

        if (currentTime > decoded.exp) {
            localStorage.removeItem('cefimtoken');
            window.location = "/login";
            return;
        } else {
            this.setState({loaded : true});
            this.setState({jwtToken : { 
                email : decoded.sub,
                role : decoded.roles[0].authority}});
        }
    }

    componentDidMount() {
        const jwtCefimToken = getCefimJwt();

        axios({
            method: 'GET',
            url: 'http://localhost:8080/api/v1/stagesVisibled/',
            headers: {'Authorization' : jwtCefimToken} 
          }).then((response ) => {
              this.setState({
                  stageData : response.data
              })
              
          }).catch((error) => {
            // Error
            if (error.response) {
                console.log(error.response);         
                console.log(error.response.data)
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });    
    }

    render() {
    
        if (!this.state.loaded) {
            return(
                <div className="loader">
                    <Loader 
                    type="ThreeDots"
                    color="orange"
                    height="100"	
                    width="100"
                    />  
                </div>
    
               );
        } else {

            return (
            <div>
                <header className="stagesHeader">
                        
                    <img src ={cefimLogo} alt="CEFIM Logo" className="logoCefimHomePage"/>  
                            
                        <div className="logoutProfil">
                            <UncontrolledDropdown setActiveFromChild>
                                <DropdownToggle tag="a" className="nav-link" caret>
                                    <span id="navProfil">{this.state.jwtToken.email} </span>
                                </DropdownToggle>
                                <DropdownMenu>
                                    <DropdownItem tag="a" href="/login" active onClick={this.handleLogout}>Déconnexion</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </div>
                    </header>
                
                <Container className="stagesContainer">
                    <Row>   
                        <Col sm="12" md={{ size: 8, offset: 2 }}> 
                            <span id="stagesTitle">Liste de stages</span>
                    {
                        this.state.stageData.map((stage) =>
                        <div key={stage.id}>
                            <Card>
                                <CardBody>
                                    <CardTitle id="jobTitle">{stage.jobTitle} <span id="publishedDate">Publié le {stage.publishedDate}</span></CardTitle>
                                    <CardSubtitle id="companyNameCity">{stage.companyName} - {stage.companyCity}</CardSubtitle>
                                    <CardText>
                                        <span id="jobDescription">Description</span><hr />
                                        {stage.jobDescription}
                                        </CardText>
                                    <CardText><strong>Contact : </strong><a href="mailto:{stage.companyContactEmail}" id="emailContact">{stage.companyContactEmail}</a></CardText>
                                </CardBody>
                            </Card>
                            <br />
                        </div>
                        )  
                    }   
                        </Col>
                    </Row>
                </Container>                    
            </div>
            )
            }
    }
}

export default Protected