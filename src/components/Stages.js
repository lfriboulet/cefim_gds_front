import React from 'react';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";
import {Table, Container, Row, Col} from 'reactstrap';
import ModalAddStage from './ModalAddStage';
import ModalModifyStage from './ModalModifyStage';
import ModalDeleteStage from './ModalDeleteStage';
import ModalVisibleStage from './ModalVisibleStage';
import ModalInVisibleStage from './ModalInvisibleStage';

export default class Stages extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          stageData : [],
        };
        this.getStagesData = this.getStagesData.bind(this);
    }

    getStagesData() {
            axios({
                method: 'GET',
                url: 'http://localhost:8080/api/v1/stages',
                headers: {'Authorization' : getCefimJwt()} 
                }).then((response ) => {
                    this.setState({
                        stageData : response.data
                    })
                }).catch((error) => {
                if (error.response) {
                    console.log(error.response);         
                    console.log(error.response.data)
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
      }

      componentDidMount() {
        this.getStagesData();
      }

      render() {
        
          return ( 
                <div>
                    <Container>
                        <Row>
                            <Col sm="12" md={{ size: 12, offset: 0 }}>
                            <h2>Liste des stages</h2>
                            <hr />     
                            <ModalAddStage getStagesData={this.getStagesData} /> 
                            <br />
                            <Table className="table table-striped table-hover table-bordered">
                                <thead>
                                <tr className="table-active">
                                    <th scope="col">Offre</th>
                                    <th scope="col">Titre</th>
                                    <th scope="col">Société</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                        this.state.stageData.map((stage, index) =>
                        
                                <tr key={stage.id}>
                                
                                    <td>{stage.id}</td>
                                    <td>{stage.jobTitle}</td>
                                    <td>{stage.companyName}</td>
                                    <td className="actions">
                                        {
                                        (this.state.stageData[index].visibled)
                                        ?
                                        <ModalInVisibleStage data={this.state.stageData[index]} getStagesData={this.getStagesData} /> 
                                        :
                                        <ModalVisibleStage data={this.state.stageData[index]} getStagesData={this.getStagesData} />
                                        }
                                        <span> <ModalModifyStage data={this.state.stageData[index]} getStagesData={this.getStagesData} /> </span>
                                        <span> <ModalDeleteStage data={this.state.stageData[index]} getStagesData={this.getStagesData} />  </span>
                                    </td>
                                </tr>
      )     
  }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                 </Container>  
            </div>  
          
          )
      }


  
 
}