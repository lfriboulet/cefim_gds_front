import React from 'react';
import {Table, Container, Row, Col} from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";
import ModalAddTraining from './ModalAddTraining';
import ModalModifyTraining from './ModalModifyTraining';
import ModalDeleteTraining from './ModalDeleteTraining';


export default class Trainings extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          trainingData : []
        };
        this.getTrainingsData = this.getTrainingsData.bind(this);
    }
    
    getTrainingsData() {

        axios({
          method: 'GET',
          url: 'http://localhost:8080/api/v1/trainings',
          headers: {'Authorization' : getCefimJwt()} 
          }).then((response ) => {
              this.setState({
                trainingData : response.data
              });
            
          }).catch((error) => {
          
          if (error.response) {
              console.log(error.response);         
              console.log(error.response.data)
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              console.log(error.request);
          } else {
              console.log("Error ---->", error.message);
          }
          console.log(error.config);
      });
      }

      componentDidMount() {
        this.getTrainingsData();
      }

      render() {

          return(
             
            <div>
             <Container>
             <Row>
                <Col sm="12" md={{ size: 12, offset: 0 }}>
            <h2>Liste des formations</h2>
            <hr />     
             <ModalAddTraining trainingData={this.state.trainingData} getTrainingsData={this.getTrainingsData} /> 
             <br />
                <Table className="table table-striped table-hover table-bordered">
                    <thead>
                        <tr className="table-active">
                            <th scope="col">Id</th>
                            <th scope="col">Nom de la formation</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>  
                   
                    {
                      
                            this.state.trainingData.map((training, index) =>

                            <tr key = {training.id}>
                                <td>{training.id}</td>
                                <td>{training.trainingName}</td>
                                <td className="actions"> 
                                    
                                    <ModalModifyTraining dataTraining={this.state.trainingData[index]} getTrainingsData={this.getTrainingsData} /> 
                                   <span>
                                    <ModalDeleteTraining dataTraining={this.state.trainingData[index]} getTrainingsData={this.getTrainingsData}/> 
                                   </span>
                                </td>
                            </tr>      
                                )   
                        }
                    </tbody>
                </Table>   
                </Col>
                </Row>
                 </Container>  
            </div>  
          )
      }
}