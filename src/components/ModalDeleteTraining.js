import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalDeleteTraining extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            modal : false,
            trainingForm : this.props.dataTraining
        }
    
        this.toggle = this.toggle.bind(this);
        this.handleTrainingDelete = this.handleTrainingDelete.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    handleTrainingDelete() {
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
        }
       
        
        axios({
            method: 'DELETE',
            url: 'http://localhost:8080/api/v1/training/' + this.state.trainingForm.id,
            headers: {'Authorization' : jwtCefimToken},
           
            }).then((response ) => {
            this.props.getTrainingsData();
            this.toggle();
                
            }).catch((error) => {
            
            if (error.response) {
                console.log(error.response);         
                console.log(error.response.data)
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });
    } 

    render() {
        return (
                  <div>
        <Button color="danger" onClick={this.toggle}>Supprimer</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Suppression de la formation {this.state.trainingForm.trainingName}</ModalHeader>
          <ModalBody>
            <h6>Confirmer la suppression du stage <strong>{this.state.trainingForm.trainingName}</strong> ? </h6>
          </ModalBody>
          <ModalFooter>
            <Button color="info" onClick={this.handleTrainingDelete}>Supprimer</Button>
            <Button color="secondary" onClick={this.toggle}>Annuler</Button>
          </ModalFooter>
        </Modal>
      </div>
        )
    }
}

export default ModalDeleteTraining