import React from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Input, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalModifyTraining extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal : false,
          trainingForm : this.props.dataTraining,
          errorTraining : '',
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleTrainingUpdate = this.handleTrainingUpdate.bind(this);
      }

      handleChange(event) {
        this.setState({
            trainingForm : { 
                ...this.state.trainingForm,
                [event.target.name] : event.target.value
            }
        });
      }

      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

      handleTrainingUpdate(event) {
        event.preventDefault();
        const jwtCefimToken = getCefimJwt();
        if(!jwtCefimToken) {
            this.props.history.push('/login');
         }
     
        axios({
            method: 'PUT',
            url: 'http://localhost:8080/api/v1/training/' + this.state.trainingForm.id,
            headers: {'Authorization' : jwtCefimToken},
            data : {
                id : this.state.trainingForm.id,
                trainingName: this.state.trainingForm.trainingName,
                actived : this.state.trainingForm.actived
                }           
            }).then((response ) => {
            this.props.getTrainingsData();
            this.toggle();
            }).catch((error) => {
            if (error.response) {
              this.setState( {
                ...this.state,
                errorTraining : error.response.data.message 
            } )
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log("Error ---->", error.message);
            }
            console.log(error.config);
        });
    }

    render() {
        return (
          <div>
        
            <Button color="warning" onClick={this.toggle}>Modifier</Button>
            <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
              toggle={this.toggle} className={this.props.className}>
              <ModalHeader toggle={this.toggle}>Modification de la formation {this.state.trainingForm.trainingName} </ModalHeader>
              <Form onSubmit={this.handleTrainingUpdate}>
              <ModalBody>
                
              {(this.state.errorTraining !== '') ? <Alert color="danger">{this.state.errorTraining}</Alert> : ''}

              <Label for="trainingName">Formation</Label>
              <Input type="text" name="trainingName" id="trainingName" required onChange={this.handleChange} value={this.state.trainingForm.trainingName}/>
              
              <Label for="actived">Activée</Label> 
              <Input type="select" name="actived" id="actived" required onChange={this.handleChange} value={this.state.trainingForm.actived}>
                <option value="true">Activer</option>
                <option value="false">Désactiver</option>
              </Input>                   
              </ModalBody>

              <ModalFooter>
                <Button color="info">Mise à jour</Button>
                <Button color="secondary" onClick={this.toggle}>Annuler</Button>
              </ModalFooter>
              </Form>
            </Modal>
          </div>
         
        );
      }


}

export default ModalModifyTraining