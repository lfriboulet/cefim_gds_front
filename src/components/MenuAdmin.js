import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import Loader from 'react-loader-spinner';
import '../css/MenuAdmin.css';
import cefimLogo from '../img/cefimLogo.png';
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import jwt_decode from 'jwt-decode';

export default class MenuAdmin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded :false,
            jwtToken : {email : '',
                        role : ''
                    }
        }
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        const jwtCefimToken = getCefimJwt();
        if(jwtCefimToken) {
            localStorage.removeItem('cefimtoken');
            window.location = "/login";
        }
    }

    componentWillMount() {

        const jwtCefimToken = getCefimJwt();

        if (jwtCefimToken === null) {
            window.location = "/login";
            return;
        } 

        const decoded = jwt_decode(jwtCefimToken);
        const currentTime = Date.now().valueOf() / 1000;

        if (currentTime > decoded.exp) {
            localStorage.removeItem('cefimtoken');
            window.location = "/login";
            return;
        } else {
            this.setState({loaded : true});
            this.setState({jwtToken : { 
                email : decoded.sub,
                role : decoded.roles[0].authority}});
        }
    }

    render() {
        
        if (!this.state.loaded) {
            return(
                <div className="loader">
                    <Loader 
                    type="ThreeDots"
                    color="orange"
                    height="100"	
                    width="100"
                    />  
                </div>
               );
        } else {
        
        return (

            <header>
                <img src ={cefimLogo} alt="CEFIM Logo" className="logoCefimHomePage"/>
                <nav>
                    <NavLink exact to="/stages" activeStyle={{fontWeight: "bold",color: "orange"}}><span className="menuItems">Stages</span></NavLink>
                    <NavLink to="/users" activeStyle={{fontWeight: "bold",color: "orange"}}><span className="menuItems">Utilisateurs</span></NavLink>
                    <NavLink to="/formations"activeStyle={{fontWeight: "bold",color: "orange"}}><span className="menuItems">Formations</span></NavLink>
                </nav>
                <UncontrolledDropdown setActiveFromChild>
                                        <DropdownToggle tag="a" className="nav-link" caret>
                                            <span id="navProfil">{this.state.jwtToken.email}</span>
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem tag="a" href="/login" active onClick={this.handleLogout}>Déconnexion</DropdownItem>
                                        </DropdownMenu>
                </UncontrolledDropdown>
            </header>
        )
        }
    }
} 

