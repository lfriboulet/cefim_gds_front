import React, { Component } from 'react';
import '../css/Home.css';
import cefimLogo from '../img/cefimLogo.png';
import {Link} from 'react-router-dom';
import ModalAddStagePartner from './ModalAddStagePartner';

class PostStage extends Component {
    constructor() {
        super()
    this.toggle = this.toggle.bind(this);
}

toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

render() {
    return (
        <div className="home">
            <div className="navBar">
                <img src ={cefimLogo} alt="CEFIM Logo" className="logoCefimHomePage"/>
                <Link to="/login"><button className="buttonLogin">Login</button></Link>
            
            </div>
                
        <div className="contentHome">
            <h3>Bienvenue sur notre application Gestion de Stages </h3>
            <br />
            <h4>Vous pouvez déposer votre offre de stage </h4>
            <br />
            <ModalAddStagePartner />
        </div>
    </div>
        );
    }
}

export default PostStage;


