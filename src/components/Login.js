import React, { Component } from 'react';
import '../css/Login.css';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import {getCefimJwt} from '../helpers/jwt';
import {Alert, Form, FormGroup, Label, Input, Button, Container, Row, Col} from 'reactstrap';
import cefimLogo from '../img/cefimLogo.jpeg';


class Login extends Component {
constructor(props) {
    super(props)

    this.state = { credentials : {
        email : '',
        password : ''
        },
        jwtToken : {
            email : '',
            role : ''
        },
        errorLogin : ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
}


handleChange(event) {
    this.setState({
        credentials : { 
            ...this.state.credentials,
            [event.target.name] : event.target.value
        }
    });
}

handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8080/login', this.state.credentials)
    .then(res => {
        localStorage.setItem('cefimtoken', res.headers.authorization);
        const token =  getCefimJwt();
 
        const decoded = jwt_decode(token);
        this.setState({jwtToken : { email : decoded.sub,
                                    role : decoded.roles[0].authority}});


        if (this.state.jwtToken.role === 'ROLE_ADMIN') {
            this.props.history.push('/stages')
        } else {
            this.props.history.push('/protected');
        }
        
        }).catch(error => {
            this.setState({errorLogin : error.message});
            this.setState({
                credentials : {
                    ...this.state.credentials,
                    password : ''
                }
            });
        }) 
    }
    


render() {
    return (
        <div>
            <Container>
                <Row>   
                    <Col sm="12" md={{ size: 6, offset: 3 }}> 
                        <img src={cefimLogo} className="img-fluid" alt="Responsive"></img>
                        <div className="loginForm">
                            <div className="loginFormStyle">
                        <p className="h3">Identification</p>
                        {
                            (this.state.errorLogin !== '')
                            ?
                            <div>
                            <br/>
                            <Alert color="danger">
                            Adresse email ou mot de passe invalide !
                            </Alert> 
                        </div>
                     :
                     <span></span>
                        }
                        <Form onSubmit = {this.handleSubmit}>
                            <FormGroup>
                                <Label for="email">Adresse email</Label>
                                <Input type="email" name="email" id="email" placeholder="Votre adresse email" onChange={this.handleChange} value = {this.state.credentials.email}/>
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Mot de passe</Label>
                                <Input type="password" name="password" id="password" placeholder="Votre mot de passe" onChange={this.handleChange} value = {this.state.credentials.password}/>
                            </FormGroup>
                            <Button color="danger">Connexion</Button>
                        </Form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
        
    )
}
}

export default Login;