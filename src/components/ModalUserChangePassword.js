import React from 'react';
import { Form, Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Input, Alert } from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";

class ModalUserChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          modal : false,
          password : '',
          confirmPassword : '',
          errorpassword : '',
          userForm : this.props.userData
            } 

        this.handleChange = this.handleChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleChangeUserPassword = this.handleChangeUserPassword.bind(this);
        };

        handleChange(event) {
            this.setState({
                    ...this.state,
                    [event.target.name] : event.target.value
            });
          }

          toggle() {
            this.setState({
              modal: !this.state.modal
            });
          }


          handleChangeUserPassword(event) {
        
            event.preventDefault();
            const jwtCefimToken = getCefimJwt();
            if(!jwtCefimToken) {
                this.props.history.push('/login');
             }
         
            axios({
                method: 'PUT',
                url: 'http://localhost:8080/api/v1/auth/changePassword/' + this.state.userForm.id,
                headers: {'Authorization' : jwtCefimToken},
                data : {
        
                        password : this.state.password,
                        confirmPassword : this.state.confirmPassword
                        
                    }           
                }).then((response ) => {
                
                this.toggle();
                    
                }).catch((error) => {
                
                if (error.response) {

                    //console.log(error.response.data.message)
                    this.setState( {
                        ...this.state,
                        errorpassword : error.response.data.message 
                    } )

                } else if (error.request) {
                    console.log(error.request);
                } else {
                    console.log("Error ---->", error.message);
                }
                console.log(error.config);
            });
        }

        render() {
            return (
                <div> 
                
                
                  <Button color="secondary" onClick={this.toggle}>Reset password</Button>
                  <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                    toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Reset password de l'utilisateur {this.state.userForm.firstname} {this.state.userForm.lastname}  </ModalHeader>
                  <Form onSubmit={this.handleChangeUserPassword}>
                    <ModalBody>
                   {(this.state.errorpassword !== '') ? <Alert color="danger">{this.state.errorpassword}</Alert> : ''}
                        
                    
                   

                           
                    <Label for="password">Mot de passe</Label>
                    <Input type="password" name="password" id="password" required value={this.state.password} onChange={this.handleChange}/>   
  
                    <Label for="confirmPassword">Confirmer mot de passe</Label>
                    <Input type="password" name="confirmPassword" id="confirmPassword" required value={this.state.confirmPassword} onChange={this.handleChange}/>  
            
                    </ModalBody>
      
                    <ModalFooter>
                      <Button color="info">Mise à jour</Button>
                      <Button color="secondary" onClick={this.toggle}>Annuler</Button>
                    </ModalFooter>
                    </Form>
                  </Modal>
                 
                </div>
              );
        }

}

export default ModalUserChangePassword