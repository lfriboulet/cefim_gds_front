import React from 'react';
import {Table, Container, Row, Col} from 'reactstrap';
import {getCefimJwt} from '../helpers/jwt';
import axios from "axios";
import ModalAddUserStagiaire from './ModalAddUserStagiaire';
import ModalAddUserFormateur from './ModalAddUserFormateur';
import ModalModifyUser from './ModalModifyUser';
import ModalDeleteUser from './ModalDeleteUser';
import ModalUserChangePassword from './ModalUserChangePassword';

export default class Users extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          userData : [],
          dropdownOpen: false
        };
        this.getUsersData = this.getUsersData.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }
    
    getUsersData() {
  
        axios({
          method: 'GET',
          url: 'http://localhost:8080/api/v1/auth/users',
          headers: {'Authorization' : getCefimJwt()} 
          }).then((response ) => {
              this.setState({
                userData : response.data
              })
              
          }).catch((error) => {
          
          if (error.response) {
              console.log(error.response);         
              console.log(error.response.data)
              console.log(error.response.status);
              console.log(error.response.headers);
          } else if (error.request) {
              console.log(error.request);
          } else {
              console.log("Error ---->", error.message);
          }
          console.log(error.config);
      });
      }

      componentDidMount() {
        this.getUsersData();
      }

      render() {

          return(
             
            <div>
             <Container>
                <Row>
                    <Col sm="12" md={{ size: 12, offset: 0 }}>
                    <h2>Liste des utilisateurs</h2>
                    <hr />     
                    
                    <div className="actions">
                        <ModalAddUserStagiaire getUsersData={this.getUsersData}/>
                        <span><ModalAddUserFormateur getUsersData={this.getUsersData}/></span>
                    </div>
                
                        <br />
                            <Table className="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr className="table-active">
                                        <th scope="col">Id</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Prénom</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>  
                            
                                {
                                
                                        this.state.userData.map((user, index) =>
                                        //utilisa tion d'un ternaire dand le but de ne pas affciher le bouton delete et modifier 
                                        // du compte administrator GDS
                                        (user.id !== 1) 
                                        ?     
                                        <tr key = {user.id}>
                                            <td>{user.id}</td>
                                            <td>{user.lastname}</td>
                                            <td>{user.firstname}</td>
                                            <td>{user.email}</td>
                                            <td>{user.roles[0].roleName === 'ROLE_USER' ? 'STAGIAIRE' : <strong>FORMATEUR</strong> }</td>
                                            <td className="actions"> 
                                                
                                            <ModalModifyUser userData={this.state.userData[index]} getUsersData={this.getUsersData} /> 
                                            <span>
                                                <ModalDeleteUser userData={this.state.userData[index]} getUsersData={this.getUsersData}/> 
                                            </span>
                                            <span>
                                            <ModalUserChangePassword userData={this.state.userData[index]} getUsersData={this.getUsersData}/>
                                            </span>
                                            </td>
                                        </tr>    
                                        // on affiche seulement le bouton reset password pour le compte administrator GDS
                                          :
                                          <tr key = {user.id}>
                                            <td><span className="superAdmin">{user.id}</span></td>
                                            <td><span className="superAdmin">{user.lastname}</span></td>
                                            <td><span className="superAdmin">{user.firstname}</span></td>
                                            <td><span className="superAdmin">{user.email}</span></td>
                                            <td><span className="superAdmin">{user.roles[0].roleName === 'ROLE_USER' ? 'STAGIAIRE' : <strong>FORMATEUR</strong> }</span></td>
                                            <td className="actions">     
                                            <ModalUserChangePassword userData={this.state.userData[index]} getUsersData={this.getUsersData}/>
                                            </td>
                                        </tr>  
                                          )
                                    }
                                </tbody>
                            </Table>   
                        </Col>
                    </Row>
                 </Container>  
            </div>  
          )
      }
}